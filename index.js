const express = require("express");
const app = express();
const mongoose = require("mongoose");
const port = 3001;

// mongodb connection

mongoose.connect("mongodb+srv://admin:admin123@zuitt.8qw2w8u.mongodb.net/B217_to_do?retryWrites=true&w=majority" , {
	useNewUrlParser : true,
	useUnifiedTopology : true
});

let db = mongoose.connection;
db.on("error", console.error.bind(console,"connection error"));

db.once("open", () => console.log("We're connected to the cloud database"));

// mongoose schemas- determines the structure of our documents to be stored in the database

 const taskSchema = new mongoose.Schema({
 	name : {
 		type: String,
 		required: [true, "Task name is required"]
 	},
 	status: {
 		type: String,
 		default: "pending"
 	}
 });

 //  model

 const Task = mongoose.model("Task", taskSchema);

const userSchema = new mongoose.Schema({
	username : {
		type: String,
		required: [true, "Username is required"]
	},
	password: {
		type: String,
		required: [true, "Password is required"]
	}
});
const User = mongoose.model("User", userSchema)

// creation of to do list routes

app.use(express.json());

app.use(express.urlencoded({extended:true}));

// post metod

 app.post("/tasks", (req, res) => {
 	Task.findOne({name: req.body.name}, (err, result) =>{
 		if(result != null && result.name == req.body.name){
			return res.send("Duplicate found");
 		}else{
 			let newTask= new Task({
 				name: req.body.name
 			});

 			newTask.save((saveErr, saveTask) => {
 				if(saveErr){
 					return console.error(saveErr);
 				}else{
 					return res.status(201).send("New task created.");
				}
			})
		}
	})
 })

app.post("/signup", (req, res) => {
	User.findOne({username: req.body.username}, (err, result) =>{
		if(result != null && result.username == req.body.username){
			return res.send("Duplicate found");
		}else{
			let newUser= new User({
				username: req.body.username,
				password: req.body.password
			});

			newUser.save((saveErr, saveUser) => {
				if(saveErr){
					return console.error(saveErr);
				}else{
					return res.status(201).send("New user registered.");
				}
			})
		}
	})
})

// get method 

app.get("/task", (req,res) => {
	Task.find({} , (err,result) => {
		if(err){
			return console.log(err);
		}else{
			return res.status(200).json({
				data : result
			});
		}
	})
})



app.listen(port,() => console.log(`Server running at localhost:${port}`));